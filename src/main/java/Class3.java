public class Class3 {

    public void method1() {
        //this method is added by DrLukas at the same time DrMonia works on method2
    }

    public void method2() {
        // this method is createdby DrMonia at the same timeDrLukas works on method1
    }

    public void method3() {
        // this method is createdby DrMonia after DrLukas resolved conflict of two commits on the sameclass,
        // but DrMonia didn't pull this change before commiting this new method

    }
}
